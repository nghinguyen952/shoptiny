<?php

namespace App\Http\Controllers;

use App\Models\DanhMuc;
use Illuminate\Http\Request;

class DanhMucController extends Controller
{
    public function index(){
        return view('admin.pages.danh_muc.index');
    }

    public function getData()
    {
        $danhMuc    = DanhMuc::all();

        return response()->json([
            'data'      => $danhMuc,
        ]);
    }

    public function getNewData()
    {
        $danhMuc = DanhMuc::find(1);

        return response()->json(['product' => $danhMuc]);
    }

    public function updateStatus($id)
    {
        $danhMuc = DanhMuc::find($id);

        if($danhMuc) {
            // $danhMuc->is_open = !$danhMuc->is_open;
            $danhMuc->is_open = $danhMuc->is_open == 0 ? 1 : 0;
            $danhMuc->save();

            return response()->json([
                'status'  => true,
            ]);

        } else {
            return response()->json([
                'status'  => false,
            ]);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        DanhMuc::create($data);

        return response()->json([
            'status'    => 1,
        ]);
    }

    public function edit($id)
    {
        $danhMuc = DanhMuc::find($id); // Trả về 1 object
        if($danhMuc) {
            return response()->json([
                'status'    => true,
                'data'      => $danhMuc,
            ]);
        } else {
            return response()->json([
                'status'    => false,
            ]);
        }
    }


    public function update(Request $request)
    {
        $danhMuc = DanhMuc::find($request->id);

        $danhMuc->ten_danh_muc      = $request->ten_danh_muc;
        $danhMuc->ma_danh_muc       = $request->ma_danh_muc;
        $danhMuc->is_open           = $request->is_open;

        $danhMuc->save();
    }


    public function destroy($id)
    {
        $danhMuc = DanhMuc::find($id);
        if($danhMuc) {
            $danhMuc->delete();

            return response()->json([
                'status'    => true,
            ]);
        } else {
            return response()->json([
                'status'    => false,
            ]);
        }
    }
}
