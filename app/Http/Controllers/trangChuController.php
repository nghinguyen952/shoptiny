<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class trangChuController extends Controller
{
    public function login(){
        return view('login');
    }
    public function loginAction(Request $request)
    {
        if(Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ],  $request->input('remember')))
        {
            return redirect('/admin/pages/trangchuAdmin');
        }

        return redirect()->back();

    }
}
