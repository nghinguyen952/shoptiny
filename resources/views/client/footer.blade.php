<!-- Grid container -->
<footer class="page-footer font-small-stylish-color-dark pt-4 text-light" id="contactus">
    <div class="container text-center text-md-left">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-md-4 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Shingu</h5>
                <p>+676767676767 (Whatsapp)</p>
                <p>Shitiny (Skype)</p>
                <p>support@tinyshop.com</p>

            </div>
            <hr class="clearfix w-100 d-md-none">
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-2 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Link hữu ích</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Giới thiệu</a>
                    </li>
                    <li>
                        <a href="#!">Blog</a>
                    </li>
                    <li>
                        <a href="#!">Chi nhánh</a>
                    </li>
                    <li>
                        <a href="#!">Lời chứng thực</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
            <hr class="clearfix w-100 d-md-none">
            <!--Grid column-->
            <div class="col-md-2 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Hỗ trợ</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Hỗ trợ người dùng</a>
                    </li>
                    <li>
                        <a href="#!">Tin tức</a>
                    </li>
                    <li>
                        <a href="#!">Điều khoản và điều kiện</a>
                    </li>
                    <li>
                        <a href="#!">Chính sách bảo mật</a>
                    </li>
                </ul>
            </div>

            <hr class="clearfix w-100 d-md-none">
            <!--Grid column-->
            <div class="col-md-2 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Follow Us</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Tinyshop</a>
                    </li>
                    <li>
                        <a href="#!">Tinyshop</a>
                    </li>
                    <li>
                        <a href="#!">Tinyshop</a>
                    </li>
                    <li>
                        <a href="#!">Tinyshop</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <hr>
    <ul class="list-unstyled list-inline text-center py-2">
        <li class="list-inline-item">
            <h5 class="mb-1">Đăng ký miễn phí</h5>
        </li>
        <li class="list-inline-item">
            <a href="#!" class="btn btn-danger btn-rounded">Đăng ký</a>
        </li>
    </ul>
    <hr>
    <ul class="list-unstyled list-inline text-center">
        <li class="list-inline-item">
            <a class="btn-floating btn-fb mx-1">
                <i class="fab fa-facebook-f"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-tw mx-1">
                <i class="fab fa-twitter"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-gplus mx-1">
                <i class="fab fa-google-plus-g"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-li mx-1">
                <i class="fab fa-linkedin-in"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-dribbble mx-1">
                <i class="fab fa-dribbble"> </i>
            </a>
        </li>
    </ul>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3"> &copy; 2022 Copyright:
        <a href="#">tinyshop@gmail.com</a>
    </div>
    <!-- Copyright -->
</footer>
