<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Tinyshop</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">

                <li class="nav-item">
                    <a class="nav-link" href="#exchange">Quy đổi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#rates">Tỉ Giá</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#service">Sản phầm</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#signup">Lịch sử</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#discover">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#discover">Chi nhánh</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contactus">Liên hệ</a>
                </li>
            </ul>
            <button class="btn btn-success text-dark" type="submit"><a href="/admin/login">Đăng
                    nhập</a></button>
            <button class="btn btn-success text-dark" type="submit">Đăng ký</button>
        </div>
    </div>
</nav>
<div class="mid">
    <video autoplay muted loop>
        <source class="embed-responsive" src="1.mp4" type="video/mp4">
    </video>
    <div class="hero text-center">
        <h2 class="text-light display-4 font-weight-bold">Tinyshop là một website chuyên cung cấp dịch vụ mua
            bán ngoại tệ</h2>
        <p class="text-light mx-auto">Chúng tôi có thể giúp bạn Gửi, Nhận, Trao đổi, Thanh toán mua sắm hoặc
            chấp nhận thanh toán trực tuyến một cách dễ dàng trên tài khoản cá nhân của bạn.</p>
        <a class="text-dark" href="#">Bắt đầu</a>
    </div>
</div>
