<section class="bg-info" id="app">
    <div class="row align-items-center container pt-5 mx-auto">
        <div class="text col-lg-12 col-md-12 col-12  pb-5">
            <h2 style="text-align: center;">Quy đổi tiền tệ</h2>
            <h6 style="text-align: center;">Không giới hạn quy đổi</h6>
            <form class="align-items-center container my-5 mx-auto w-50" action="index.php" method="get">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Lượng VNĐ muốn quy đổi:</label>
                    <input type="text" class="form-control" name="input" require>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Quy đổi sang tiền tệ:</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="dropdown">
                        <option value="rmb">RMB</option>
                        <option value="usd">USD</option>
                        <option value="gbp">EURO</option>
                    </select>
                </div>

                <div class="form-group">
                    <input style="padding: 5px; border-radius: 5px;" class="btn btn-success" type="submit"
                        name="sbmt" value="Convert">
                </div>
                <div class="form-group">

                    <?php
                    if (isset($_GET['sbmt'])) {
                        $cc_input = $_GET['input'];
                        $cc_dropdown = $_GET['dropdown'];
                        if ($cc_input != null) {
                            if ($cc_dropdown == 'rmb') {
                                $output = $cc_input * 0.00029;
                                echo '<h1>' . $output . ' RMB' . '</h1>';
                            } elseif ($cc_dropdown == 'usd') {
                                $output = $cc_input * 0.000041;
                                echo '<h1>' . $output . ' USD' . '</h1>';
                            } elseif ($cc_dropdown == 'gbp') {
                                $output = $cc_input * 0.00864463;
                                echo '<h1>' . $output . ' Euro' . '</h1>';
                            }
                        }
                    }
                    ?>

                </div>
            </form>
        </div>
    </div>

</section>

<section id="rates" class="rates py-5">
    <div class="col mx-auto align-items-center my-5">
        <div class="heading text-center pt-5">
            <h2 class="font-weight-bold pb-5 text-light">Tỉ giá của tinyshop
            </h2>
        </div>
        <div class="row mx-auto justify-content-center align-items-center text-center container">
            <div class="one col-lg-3 col-md-3 col-12 w-25 m-2">
                <img class="img-fluid w-75" src="5.jpg">
                <h5 class="font-weight-bold pt-4">USD</h5>
                <p>0.00029</p>
            </div>
            <div class="one col-lg-3 col-md-3 col-12 w-25 m-2">
                <img class="img-fluid w-75" src="6.jpg">
                <h5 class="font-weight-bold pt-4">Euro</h5>
                <p>0.00864463</p>
            </div>
            <div class="one col-lg-3 col-md-3 col-12 w-25 m-2">
                <img class="img-fluid w-75" src="7.jpg">
                <h5 class="font-weight-bold pt-4">RMB</h5>
                <p>0.00029</p>
            </div>
        </div>
    </div>
</section>

<section id="discover" class="discovery py-5">
    <div class="row align-items-center container my-5 mx-auto">
        <div class="img col-lg-6 col-md-6 col-12 w-50 pt-2 pb-5">
            <img class="img-fluid" src="FAQ.svg">
        </div>
        <div class="text col-lg-6 col-md-6 col-12 w-50 pt-2 pb-5">
            <h2>Câu hỏi cho chúng tôi</h2>
            <h6>Trong phần này, bạn có thể biết thông tin chi tiết về hệ thống Tinyshop, nếu sau khi đọc tài liệu
                này, bạn vẫn còn bất kỳ vấn đề hoặc câu hỏi nào về hệ thống, vui lòng liên hệ với chúng tôi.</h6>

            <h5 onclick="myFunction1()" class="bg-info">Tinyshop là gì? </h5>


            <p id="myfirstDIV" style="display: none;" class="bg-warning">Tinyshop là một dịch vụ trao đổi tiền
                điện tử đơn giản và nhanh chóng dành cho dịch vụ tự do. Bạn chỉ cần đăng ký và xác minh. Chúng tôi
                sẽ nhanh chóng chuyển đổi giao dịch cho bạn mà không tính thêm bất kỳ khoản phí ẩn hoặc phí nào.</p>

            <h5 onclick="myFunction2()" class="bg-info">Sẽ mất bao lâu để xử lý đơn đặt? </h5>

            <p id="mysecondDIV" style="display: none;" class="bg-warning">Thông thường, chúng tôi mất 5 phút và
                tối đa là 20 phút để hoàn thành đơn đặt hàng. Nếu có bất kỳ trường hợp khẩn cấp nào, bạn có thể liên
                hệ với chúng tôi qua trò chuyện trực tiếp, email, hộp thư đến trang người hâm mộ FB, WhatsApp, Skype
                và imo, bạn có thể tìm thấy chi tiết liên hệ của chúng tôi tại đây.</p>

            <h5 onclick="myFunction3()" class="bg-info">Các tài liệu cần thiết để xác minh là gì? </h5>

            <p id="mythirdDIV" style="display: none;" class="bg-warning">Hộ chiếu quốc tế, Giấy phép lái xe hoặc
                Chứng minh nhân dân (chúng tôi chỉ chấp nhận thẻ căn cước địa phương từ các quốc gia thuộc khu vực)
            </p>

        </div>
    </div>
</section>
